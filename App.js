import React, { Component } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Splash from "./src/screens/Splash";
import Dashboard from "./src/screens/Dashboard";
import GetSampleDataAxios from "./src/screens/GetSampleDataAxios"

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();


export default class App extends Component {
  render() {
    return (
      <NavigationContainer independent={true}>
        <Stack.Navigator initialRouteName="Dashboard">

          <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
          <Stack.Screen name="Dashboard" component={Dashboard} options={{ headerShown: false }} />
          <Stack.Screen name="GetSampleDataAxios" component={GetSampleDataAxios} options={{ headerShown: false }} />

         
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

