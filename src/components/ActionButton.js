import React, { Component } from "react";
import {
    Text,
    StyleSheet,
    TouchableOpacity
} from "react-native";

class ActionButton extends Component {
    render() {
        return (
            <TouchableOpacity style={styles.button} onPress={this.props.onPress}>
                <Text style={styles.buttonText}>{this.props.buttonName}</Text>
            </TouchableOpacity>
        );
    }
}
export default ActionButton;

const styles = StyleSheet.create({
    button: {
        borderRadius: 6,
        backgroundColor: 'purple',
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },

    buttonText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 14

    }
});