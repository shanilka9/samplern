import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput
} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import { color } from "react-native-reanimated";

class InputText extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>{this.props.title}</Text>
                <TextInput style={styles.input}
                    secureTextEntry={this.props.secureTextEntry}
                    placeholder={this.props.placeholder}
                    placeholderTextColor={this.props.placeholderColor}
                    onChangeText={this.props.setState}
                    value={this.props.stateValue}
                    keyboardType={this.props.keyType}
                    editable={this.props.editable}
                    multiline={this.props.multiline}
                    numberOfLines={this.props.numberOfLines}
                    maxLength={this.props.max}
                >
                </TextInput>
                {/* {this.props.is_validated ? <Icon name='ios-close-circle' size={27} color={'red'} style={styles.icon} /> : null } */}
            </View>
        );
    }
}

{/*  */ }
export default InputText;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        width: '100%',
        marginBottom: 5
    },
    title: {
        margin: 2,
        color: 'purple',
        fontSize:15
        
    },
    input: {
        borderColor: 'black',
        borderWidth: 1,
        height: 50,
        borderRadius: 6,
        padding: 10
    },
    icon: {
        position: 'absolute',
        right: 12,
        bottom: 10
    }
});