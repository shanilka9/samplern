import React, { Component } from "react";
import {
    View,
    StyleSheet,
    Alert,
    Text
} from "react-native";
import InputText from "../components/InputText";
import ActionButton from "../components/ActionButton";

const regExEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const regExMobile = /^\d{10}/;
import * as firebaseSample from 'firebase';

const config = {

    apiKey: "AIzaSyAUUZSPV4Bw3qdmTTFyOvoNXF4yl5x1SMQ",
    authDomain: "samplern-ed597.firebaseapp.com",
    databaseURL: "https://samplern-ed597.firebaseio.com",
    projectId: "samplern-ed597",
    storageBucket: "samplern-ed597.appspot.com",
    messagingSenderId: "531750281828",
    appId: "1:531750281828:web:f7d188929497ac907ff571",
    measurementId: "G-641ETFTVQN"
};
if (!firebaseSample.length) {
    try {
        firebaseSample.initializeApp(config)
    } catch (err) {
        console.error("Firebase initialization error raised", err.stack);
    }
}
// Initialize Firebase
//const app = firebaseSample.initializeApp(config);
export const db = firebaseSample.database();

class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            address: '',
            contact: '',
            email: '',

        }

    }

    componentDidMount() {

        // console.log("gagana");
        //  console.log(firebaseSample.name);
        // console.log(firebaseSample.database());

        db.ref("Registration")
            .child("gagana").on('value', querySnapShot => {
                let data = querySnapShot.val();
                let todoItems = { data };
                // this.setState({
                //   todos: todoItems,

                // });
              //  console.log(data);
            });
        // console.log(todoItems);

        // db.ref("/Registration")
        //     .child("gagana")
        //     .on("value", snapshot => {
        //         const data = snapshot.val();
        //         console.log(data);
        //         if (snapshot.val()) {
        //             const initMessage = [];
        //             console.log(data);
        //         }
        //     });

        // firebaseSample.database()
        // .child("Registration").on("child_added",snapshot=>{
        //     const data=snapshot.val();
        //     if(data){
        //        // this.setState()
        //     }
        // })
    }

    validated() {
        if (this.state.username === '') {
            Alert.alert("Username Empty")
            return false;
        }
        else if (this.state.address === '') {
            Alert.alert("Address Empty")
            return false;
        }
        else if (this.state.contact === '' || regExMobile.test(this.state.contact) !== true || this.state.contact.length > 10) {
            Alert.alert("Enter Valid Contact Number")
            return false;
        }
        else if (this.state.email === '' || regExEmail.test(this.state.email) === false) {
            Alert.alert("Enter Valid Email")
            return false;
        } else {
            return true;
        }
    }

    submitData() {
        if (this.validated()) {

            db.ref("Registration")
                .child(this.state.username)
                .set({
                    Name: this.state.username,
                    Address: this.state.address,
                    ContactNo: this.state.contact,
                    Email: this.state.email

                },
                    (error) => {
                        if (error) {
                            console.log(error.message);
                        } else {
                            Alert.alert("Submit");
                        }
                    });

        }

    }



    render() {
        return (
            <View style={styles.container}>

                <Text style={{ alignSelf: 'center', color: 'purple', fontSize: 20, fontWeight: 'bold' }}>Sign Up</Text>

                <View style={{ padding: 10 }} />


                <InputText
                    title='Name'
                    placeholder='Enter Name'
                    value={this.setState.username}
                    setState={(username) => this.setState({ username })}

                />

                <InputText
                    title='Address'
                    placeholder='Enter Address'
                    value={this.setState.address}
                    setState={(address) => this.setState({ address })}

                />

                <InputText
                    title='Contact Number'
                    placeholder='Enter Contact Number'
                    value={this.setState.contact}
                    keyType='number-pad'
                    setState={(contact) => this.setState({ contact })}

                />

                <InputText
                    title='Email'
                    placeholder='Enter Email'
                    value={this.setState.email}
                    keyType='email-address'
                    setState={(email) => this.setState({ email })}
                />

                <View style={{ padding: 10 }} />

                <ActionButton
                    buttonName='Submit'
                    onPress={() => this.submitData()}
                />
<View style={{padding:5}}></View>
                <ActionButton
                    buttonName='Get Data With Axios'
                    onPress={() => this.props.navigation.navigate('GetSampleDataAxios')}
                />

            </View>
        );
    }
}
export default Dashboard;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 25
    }

});