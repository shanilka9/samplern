import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
const axios = require('axios');


class GetSampleDataAxios extends Component {
    constructor(props) {
        super(props)
        this.state = {
            users: []
        }
    }

    componentDidMount() {
        axios.get('https://reqres.in/api/users?page=2')
            .then((response) => {
                this.setState({
                    users: response.data.data
                })
            })
    }
    render() {
        return (

            <View style={styles.container}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    style={{ width: '100%' }}
                    data={this.state.users}
                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity style={{ padding: 10, flexDirection: 'row' }}>
                                <Image
                                    style={styles.avatar}
                                    source={{ uri: item.avatar }}
                                />
                                <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                                    <Text >{item.first_name}</Text>
                                    <Text >{item.last_name}</Text>
                                    <Text >{item.email}</Text>
                                </View>

                            </TouchableOpacity>
                        );
                    }}
                    keyExtractor={item => `bid_${item.id}`}
                />
            </View>


        );
    }
}
export default GetSampleDataAxios;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 25
    },
    avatar: {
        width: 50,
        height: 50,
    }
});